# Performance and Growth

This project is a way for me to track my personal performance and growth rate in a most transparent way.

### Contents

- [Credit Tracker](anna_vovchenko/performance-and-growth#1)
- [FY23-Q2 Goals](anna_vovchenko/performance-and-growth#2)
- [Weekly planning](anna_vovchenko/performance-and-growth#3)

### Statistics

<p>
  <img src="https://gitlab-readme-stats.vercel.app/api?username=anna_vovchenko&show_icons=true&hide=[%22todos%22]" />
</p>


